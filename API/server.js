  const mariadb = require('mariadb')
const express = require('express')
const app = express()
const cors = require('cors')

require('dotenv').config()

const pool = mariadb.createPool({
    host: process.env.DB_HOST,
    database : process.env.DB_DTB,
    user : process.env.DB_USER,
    password : process.env.DB_PWD
})

app.use(express.json())
app.use(cors())

app.get('/product/:id', async(req, res) =>{
    let conn;
    let id = parseInt(req.params.id)
    conn = await pool.getConnection();
    const rows = await conn.query(`SELECT * FROM product WHERE id_product = ${id};`)
    res.status(200).json(rows)
})


// Récupération des utilisateurs
app.get('/userlist', async (req, res) => {
  let conn = await pool.getConnection();
  const rows = await conn.query('SELECT fist_name, surname FROM client;');
  res.status(200).json(rows)
  conn.release();
});


app.put('/product/:id',async(req,res)=>{
    let conn;
    let id = parseInt(req.params.id)
    conn = await pool.getConnection();
    await conn.query(`UPDATE product SET titre = ?, duree = ? WHERE id = ${id}`,[req.body.titre,req.body.duree])
    const rows = await conn.query('SELECT * FROM product;')    
    res.status(200).json(rows)
})

app.delete('/product/:id', async(req, res) =>{
    let conn;
    let id = parseInt(req.params.id)
    conn = await pool.getConnection();
    await conn.query(`DELETE FROM product WHERE id = ?;`,[id])
    const rows = await conn.query(`SELECT * FROM product;`)
    res.status(200).json(rows)
})

//admin add product
app.post('/adminAddProduct', async (req, res) => {
  let conn;
  const { name, price, stock, description, id_category, img_name } = req.body;
  conn = await pool.getConnection();
  await conn.query(`INSERT INTO product (name, price, stock, description, id_category, img_name) VALUES (?, ?, ?, ?, ?, ?);`, [name, price, stock, description, id_category, img_name]);
  const rows = await conn.query(`SELECT * FROM product WHERE name = ?`, [name]);
  if (rows.length > 0) {
    res.status(200).json({
      success: true      
    });
  } else {
    res.status(401).json({
      success: false
    });
  }
  conn.release();
});

//admin edit produit
app.put('/adminEditProduct/:id', async (req, res) => {
  let conn;
  const { name, price, stock, description, id_category, img_name } = req.body;
  const { id } = req.params;
  conn = await pool.getConnection();
  await conn.query(`UPDATE product SET name=?, price=?, stock=?, description=?, id_category=?, img_name=? WHERE id_product=?;`, [name, price, stock, description, id_category, img_name]);
    res.status(200).json({
      success: true
    });
  conn.release();
});

// Récupération de tous les produits
app.get('/products', async (req, res) => {
  let conn = await pool.getConnection();
  const rows = await conn.query('SELECT * FROM product;');
  res.status(200).json(rows)
  conn.release();
});

//login/logout

app.post('/client_login', async (req, res) => {
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query(`SELECT * FROM client WHERE email = ? AND password = ?`, [req.body.email, req.body.password]);
    if (rows.length > 0) {
        const token = generateToken();
        await conn.query(`UPDATE client SET token = ? WHERE client.email = ?`, [token, req.body.email]);
        res.status(200).json({
          success: true,
          token: token.toString(),
          userData: rows[0]
        });
      } else {
        res.status(401).json({
          success: false,
          error: 'Invalid email or password',
        });
    }
    function generateToken() {
        let token = Math.random().toString(36).substring(2)+Math.random().toString(36).substring(2);
        return token;
    }
    conn.release();
});

app.post('/apilogin', async (req, res) => {
  console.log("ACCES API LOGIN");
  console.log(req.body);
  console.log("ACCES API LOGIN");
  console.log(req.body.email, req.body.password); // Vérification des valeurs
  let conn;
  conn = await pool.getConnection();
  const rows = await conn.query(`SELECT * FROM client WHERE email = ? AND password = ?`, [req.body.email, req.body.password]);
  if (rows.length > 0) {
        res.status(200).json({
        success: true
      });
      console.log(res);
    } else {
      res.status(401).json({
        success: false,
        error: 'Invalid email or password', 
      });
  }
  conn.release();
  
});

app.post('/verify_token', async (req, res) => {
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query(`SELECT * FROM client WHERE token = ?`, [req.body.localToken]);
    if (rows.length > 0) {
      res.status(200).json({
        success: true,
        userData: rows[0],
      });
    } else {
      res.status(401).json({
        success: false,
        error: 'Invalid token',
      });
    }
    conn.release();
});

app.post('/client_logout', async (req, res) => {
    let conn;
    conn = await pool.getConnection();
    await conn.query(`UPDATE client SET token = "" WHERE client.token = ?;`, [req.body.token]);
    const rows = await conn.query('SELECT * FROM client c WHERE c.idClient = ? AND token = "";', [req.body.idClient]);      
    if (rows.length > 0) {
        res.status(200).json({
          success: true,
        });
    } else {
        res.status(401).json({
          success: false,
          error: 'Invalid token',
        });
    }
    conn.release();
});


//create user
app.post('/CreateUser',async(req,res)=>{
  let conn;
  conn = await pool.getConnection();
  await conn.query('INSERT INTO client(first_name, surname, adress, email, phone, password) VALUES (?,?,?,?,?,?)',[req.body.first_name,req.body.last_name,req.body.address,req.body.email,req.body.phone,req.body.passwordTwo])
  const rows = await conn.query(`SELECT * FROM client WHERE email = ? AND password = "?"`, [req.body.email, req.body.passwordTWO]);
  if (rows.length > 0) {
    const token = generateToken();
    await conn.query(`UPDATE client SET token = ? WHERE client.email = ?`, [token, req.body.email]);
    res.status(200).json({
      success: true,
      token: token.toString(),
    });
  } else {
    res.status(401).json({
      success: false,
      error: 'Invalid infos!',
    });
  }
  function generateToken() {
    let token = Math.random().toString(36).substring(2)+Math.random().toString(36).substring(2);
    return token;
}
  conn.release();
})


//admin add user
app.post('/adminAddUser', async (req, res) => {
  let conn;
  const { first_name, surname, address, email, phone, admin } = req.body;
  conn = await pool.getConnection();
  await conn.query(`INSERT INTO client (first_name, surname, adress, email, phone, admin, password) VALUES (?, ?, ?, ?, ?, ?, ?);`, [first_name, surname, address, email, phone, admin, phone]);
  const rows = await conn.query(`SELECT * FROM client WHERE email = ? AND password = ?`, [email, phone]);
  if (rows.length > 0) {
    res.status(200).json({
      success: true      
    });
  } else {
    res.status(401).json({
      success: false
    });
  }
  conn.release();
});

//admin edit user
app.put('/adminEditUser/:id', async (req, res) => {
  let conn;
  const { first_name, surname, address, email, phone, admin } = req.body;
  const { id } = req.params;
  conn = await pool.getConnection();
  await conn.query(`UPDATE client SET first_name=?, surname=?, adress=?, email=?, phone=?, admin=? WHERE idClient=?;`, [first_name, surname, address, email, phone, admin, id]);
    res.status(200).json({
      success: true
    });
  conn.release();
});



//client

app.get('/client', async(req, res) =>{
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM client;')
    res.status(200).json(rows)
})

app.get('/client/:id', async(req, res) =>{
    let conn;
    let id = req.params.id
    conn = await pool.getConnection();
    const rows = await conn.query(`SELECT * FROM client WHERE email = "${id}";`)
    res.status(200).json(rows)
})

// userinterface 
app.post('/update_user', async (req, res) => {
    let conn;
    conn = await pool.getConnection();
    await conn.query(`UPDATE client SET last_name = ?, first_name = ?, address = ?, email = ?, phone = ?, password = ? WHERE client.idClient = ?;`, [req.body.last_name, req.body.first_name, req.body.address, req.body.email, req.body.phone, req.body.password, req.body.idClient]);
    const rows = await conn.query('SELECT * FROM client c WHERE c.last_name = ? AND c.first_name = ? AND c.adress = ? AND c.email = ? AND c.phone = ? AND c.password = ?;', [req.body.last_name, req.body.first_name, req.body.address, req.body.email, req.body.phone, req.body.password, req.body.idClient]);   
    if (rows.length > 0) {
        res.status(200).json({
          success: true,
          userData: rows[0],
        });
      } else {
        res.status(401).json({
          success: false,
          error: 'Invalid token',
        });
      }
      conn.release();
});

// getOrders
app.get('/getOrders/:idClient', async(req, res) =>{
  let conn;
  let idClient = parseInt(req.params.idClient);
  conn = await pool.getConnection();
  const rows = await conn.query(`SELECT * FROM command WHERE id_Client = ${idClient};`)
  res.status(200).json(rows)
  conn.release();
})


// Récupération de tous les utilisateurs
app.get('/users', async (req, res) => {
  let conn = await pool.getConnection();
  const rows = await conn.query('SELECT * FROM client;');
  res.status(200).json(rows)
  conn.release();
});

// Suppression d'un utilisateur par son ID
app.delete('/users/:idClient', async (req, res) => {
  let conn = await pool.getConnection();
  const idClient = req.params.idClient;
  conn.query(`DELETE FROM client WHERE idClient = ${idClient};`);
  res.status(200).json({ success: true });
  conn.release();
});

// Suppression des utilisateur selectionner par leurs ID
app.delete('/delete-selected-users', async (req, res) => {
  const { ids } = req.body;
  let conn = await pool.getConnection();
  conn.query(`DELETE FROM client WHERE idClient IN (${ids.join(',')})`);
  res.status(200).json({ success: true });
  conn.release();
});



app.listen(8000, () =>{
    console.log("Serveur à l'écoute");
} )