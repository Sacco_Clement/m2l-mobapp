import 'package:flutter/material.dart';
import 'package:flutterapp/pages/ProductList.dart';

class Menu extends StatelessWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Menu'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/liste');
          },
          child: Text('Liste des Produits'),
          style: ButtonStyle(
            minimumSize: MaterialStateProperty.all<Size>(
              Size(300, 70),
            ),
          ),
        ),
      ),
    );
  }
}
