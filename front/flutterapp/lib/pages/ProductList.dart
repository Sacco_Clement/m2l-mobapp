import 'dart:async';
import 'dart:convert';
import 'package:flutterapp/lien.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ProductListPage extends StatefulWidget {
  const ProductListPage({Key? key}) : super(key: key);

  @override
  _ProductListPageState createState() => _ProductListPageState();
}

class _ProductListPageState extends State<ProductListPage> {
  List<dynamic> productList = [];

  Future<List<dynamic>> fetchProductList() async {
    final response = await http.get(Uri.parse("http://localhost:8000/products"));
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      return result;
    } else {
      throw Exception("échec récupération des produits");
    }
  }

  @override
  void initState() {
    super.initState();
    fetchProductList().then((result) {
      setState(() {
        productList = result;
      });
    }).catchError((error) {
      print(error);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Liste des produits'),
      ),
      body: productList.isEmpty
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: productList.length,
              itemBuilder: (BuildContext context, int index) {
                final product = productList[index];
                return ListTile(
                  leading: product['stock'] == 0
                      ? Icon(Icons.block, color: Colors.red)
                      : Icon(Icons.check_circle, color: Colors.green),
                  title: Text(
                    product['name'],
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text('${product['stock']} en stock'),
                );
              },
            ),
    );
  }
}
